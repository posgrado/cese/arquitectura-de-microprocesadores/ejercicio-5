/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

#ifndef __EJERCICIO_C_FUNCTION_H__
#define __EJERCICIO_C_FUNCTION_H__

#include "ejercicio.h"

/**
 * c_function do the same of ASM function for compare purpose
 * 
 * @param * vectorIn cada elemto se multiplica x escalar
 * @param * vectorOut vector de salida
 * @param longitudVectorIn (el vector de salida queda mas chico)
 * @param escalar factor de multiplicacion
 */
void c_filtroVentana10(uint16_t * vectorIn, uint16_t * vectorOut, uint32_t longitudVectorIn);

#endif /* __EJERCICIO_C_FUNCTION_H__ */
