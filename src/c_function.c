/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

#include "c_function.h"

void c_filtroVentana10(uint16_t * vectorIn, uint16_t * vectorOut, uint32_t longitudVectorIn) {
	uint16_t med = 0;
	for (int i = 0; i < longitudVectorIn - 10; ++i) {
		med = 0;
		for (int var = 0; var < 10; ++var) {
			med += vectorIn[i + var];
		}
		med /= 10;
		vectorOut[i] = med;
	}
}

